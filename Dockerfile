FROM php:8.2-apache

WORKDIR /var/www/html

COPY . .

RUN a2enmod rewrite

RUN apt-get update \
    && apt-get install -y libpq-dev libzip-dev \
    && docker-php-ext-install pdo pdo_mysql pdo_pgsql zip

RUN echo "memory_limit=-1" > $PHP_INI_DIR/conf.d/memory-limit.ini

RUN echo "extension=pdo_mysql.so" > $PHP_INI_DIR/conf.d/pdo_mysql.ini
