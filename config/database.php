<?php

/**
 * Database connection info
 */

$dbHost = getenv("db_host") ?: 'localhost';
$dbPort = getenv("db_port") ?: '3306';
$dbName = getenv("db_name") ?: 'db';
$dbUsername = getenv("db_username") ?: 'usr';
$dbPassword = getenv("db_password") ?: '';
$dsn = "mysql:host=$dbHost;dbname=$dbName";
