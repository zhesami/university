<div class="container">
    <form action="controller/register.php" method="post">
        <div class="form-group">
            <label for="name">نام دانشجو</label>
            <input type="text" name="name" id="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="lname">نام خانوادگی دانشجو</label>
            <input type="text" name="lname" id="lname" class="form-control">
        </div>

        <div class="form-group">
            <label for="melli_code">شماره ملی</label>
            <input type="text" name="melli_code" id="melli_code" class="form-control">
        </div>

        <div class="form-group" style="display: flex;">
            <label for="diplom">مقطع تحصیلی</label>
            <input type="radio" id="diplom" name="madrak" value="diplom">
            <label for="kardani">دیپلم</label><br>
            <input type="radio" id="kardani" name="madrak" value="kardani">
            <label for="css">کاردانی</label><br>
            <input type="radio" id="karshenasi" name="madrak" value="karshenasi">
            <label for="karshenasi">کارشناسی</label>
        </div>

        <div class="form-group">
            <label for="mobile_number">شماره موبایل</label>
            <input type="text" name="mobile_number" id="mobile_number" class="form-control">
        </div>

        <div class="form-group">
            <label for="address">آدرس</label>
            <input type="text" name="address" id="address" class="form-control">
        </div>

        <div class="form-group">
            <input type="submit" value="ثبت نام" class="btn btn-blue form-control">
        </div>
    </form>
</div>