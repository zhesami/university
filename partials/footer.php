<footer>
    <p>
        دانشگاه فنی و حرفه ای استان زنجان
    </p>
    <ul>
        <li>
            <a href="https://www.facebook.com/">انجمن دانشجویان</a>
        </li>
        <li>
            <a href="https://www.twitter.com/">بسیج دانشجویی</a>
        </li>
        <li>
            <a href="https://www.instagram.com/">پدیده ها</a>
        </li>
        <li>
            <a href="https://www.instagram.com/">ورود دانشجویان</a>
        </li>
        <li>
            <a href="https://www.instagram.com/">ثبت نام دانشجویان جدید الورود</a>
        </li>
    </ul>
    <form action="mailto:info@example.com" method="post">
        <input type="text" name="name" placeholder="نام">
        <input type="email" name="email" placeholder="ایمیل">
        <input type="submit" value="ارسال">
    </form>
</footer>