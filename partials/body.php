<div class="container">
    <h3>اسامی دانشجویان:</h3>
<?php
include "config/database.php";
try {
    $connection = new PDO($dsn, $dbUsername, $dbPassword);
    $sql = "SELECT * FROM students";

    $statement = $connection->prepare($sql);
    $statement->execute();

    $students = $statement->fetchAll();

    foreach ($students as $student) {
        echo "<div class='bg-blue p-5'>" . $student["stu_id"] . " - " . $student["name"] . " " . $student["lname"] . " - " . $student["address"]. "</div>";
    }
} catch (PDOException $e) {
    echo "<p class='text-error'>امکان اتصال به دیتابیس مقدور نمیباشد.</p>";
}
?>
</div>