<div class="container">
    <div class="header">
        <div class="navbar">
            <ul>
                <li><a href="index.php">صفحه اصلی</a></li>
                <li><a href="about.php">درباره دانشگاه</a></li>
                <li><a href="masters.php">اساتید</a></li>
                <li><a href="contact-us.php">تماس با ما</a></li>
            </ul>
        </div>

        <div class="introduction">
            <h1>به پایگاه رسمی دانشگاه فنی و حرفه ای استان زنجان خوش آمدید</h1>
            <p> دانشجویان جدیدالورود بایستی تا مورخه ۱۴۰۲/۱۲/۲۹ از طریق لینک ثبت نام در سایت ثبت نام نمایند.</p>
            <p>دانشگاه هیچگونه مسئولیتی در قبال ارائه خوابگاه به دانشجویان شبانه ندارد.</p>
            <br>
            <a href="register.php" class="btn btn-blue">ثبت نام دانشجویان جدید</a>
        </div>
    </div>
</div>