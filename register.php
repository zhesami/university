<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>دانشگاه فنی و حرفه ای - ثبت نام دانشجو</title>
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <?php include "partials/header.php" ?>
    <?php include "partials/register-form.php" ?>
    <?php include "partials/footer.php" ?>
</body>

</html>